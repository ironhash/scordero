﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using shanuMVCUserRoles.Models;
using Owin;
using System.Security.Claims;
using System.Linq;

[assembly: OwinStartupAttribute(typeof(shanuMVCUserRoles.Startup))]
namespace shanuMVCUserRoles
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
			createRolesandUsers();
		}


        //Create user filling username, email, context
        public ApplicationUser createUser(string username, string email, ApplicationDbContext context)
        {
            if (!context.Users.Any(u => u.UserName == username))
            {
                var appUser = new ApplicationUser { UserName = username, Email = email };
                return appUser;
            }
            return null;
        }

        // In this method we will create default User roles and Admin user for login
        private void createRolesandUsers()
        {
            //Declarar array de roles que se van a crear
            string[] arrayroles = new string[] { "Admin", "Manager", "Employee", "testRole" };

            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            //Crear automáticamente un usuario cuando la aplicación arranque (User = a@a.com, Pwd = a@a.com)   
            var usuario = createUser("a@a.com", string.Empty, context);
            if (usuario != null) UserManager.Create(usuario, "a@a.com");


            foreach (string rol in arrayroles)
            {
                if (!roleManager.RoleExists(rol))
                {
                    var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                    role.Name = rol;
                    roleManager.Create(role);

                    if (rol == "Admin")
                    {
                        var user = createUser("info@etg.global", "info@etg.global", context);
                        if (user != null) UserManager.Create(user, "a@a.com");

                        var chkUser = UserManager.Create(user, "info@etg.global");

                        //Add default User to Role Admin
                        if (chkUser.Succeeded)
                        {
                            var result1 = UserManager.AddToRole(user.Id, rol);
                        }
                    }

                }
            }

        }
    }
}
