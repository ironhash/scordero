﻿using shanuMVCUserRoles.Controllers;
using shanuMVCUserRoles.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity;
using Unity.AspNet.Mvc;
using Unity.Injection;

namespace shanuMVCUserRoles.App_Start
{
    public static class Bootstrapper
    {
        //Register component
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            //Register dependency
            container.RegisterType<IMailService, MailService>(); 
            //Register AccountController without parameters
            container.RegisterType<AccountController>(new InjectionConstructor()); 
            return container;
        }
    }
}