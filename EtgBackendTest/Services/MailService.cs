﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace shanuMVCUserRoles.Services
{
    public class MailService : IMailService
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            string emailProveedor = ConfigurationManager.AppSettings["EmailAddress"].ToString();
            string password = ConfigurationManager.AppSettings["EmailPassword"].ToString();
            //Para poder enviar emails desde Gmail activar dicha configuracion: _https://www.google.com/settings/security/lesssecureapps

            try
            {
                using (var smtp = new SmtpClient())
                {
                    var mail = new MailMessage
                    {
                        Subject = subject,
                        From = new MailAddress(emailProveedor),
                        Body = message,
                        Priority = MailPriority.Normal,

                    };
                    mail.To.Add(email);

                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = new NetworkCredential(emailProveedor, password);
                    await smtp.SendMailAsync(mail);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Crear N emails con su n contenidos para enviarlos
        /// </summary>
        /// <returns></returns>
        public List<Email> FillContentEmails()
        {
            List<Email> listEmails = new List<Email>();

            Email c1 = new Email();
            c1.EmailAddress = "boyarenys@hotmail.com";
            c1.Message = "Contenido 1";
            listEmails.Add(c1);

            Email c2 = new Email();
            c2.EmailAddress = "saul.cordero.casas@gmail.com";
            c2.Message = "Contenido 2";
            listEmails.Add(c2);

            return listEmails;

        }
    }
}